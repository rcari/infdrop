
#include <QApplication>

#include "DropWindow.hpp"

int main( int argc, char** argv )
{
    QApplication app( argc, argv );

    DropWindow window;
    window.show();

    return app.exec();
}
