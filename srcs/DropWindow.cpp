
#include "DropWindow.hpp"

#include <QCheckBox>
#include <QDragEnterEvent>
#include <QDropEvent>
#include <QHBoxLayout>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QVBoxLayout>

#include <QtDebug>

#include <QDir>

#pragma warning( disable: 4005 ) 

#include <Windows.h>
#include <Difxapi.h>

#include <setupapi.h>
#include <strsafe.h>

#define MAX_DEVICE_ID_LEN     200
#define MAX_DEVNODE_ID_LEN    MAX_DEVICE_ID_LEN

#define MAX_GUID_STRING_LEN   39          // 38 chars + terminator null
#define MAX_CLASS_NAME_LEN    32
#define MAX_PROFILE_LEN       80

#define MAX_CONFIG_VALUE      9999
#define MAX_INSTANCE_VALUE    9999

#define MAX_MEM_REGISTERS     9     // Win95 compatibility--not applicable to 32-bit ConfigMgr
#define MAX_IO_PORTS          20    // Win95 compatibility--not applicable to 32-bit ConfigMgr
#define MAX_IRQS              7     // Win95 compatibility--not applicable to 32-bit ConfigMgr
#define MAX_DMA_CHANNELS      7     // Win95 compatibility--not applicable to 32-bit ConfigMgr

#define DWORD_MAX             0xffffffffUL
#define DWORDLONG_MAX         0xffffffffffffffffui64

#define CONFIGMG_VERSION      0x0400

DropWindow::DropWindow()
{
    setAcceptDrops( true );
    setWindowTitle( tr( "INF Drop" ) );
    setFixedSize( 250, 250 );

    QVBoxLayout* vLayout = new QVBoxLayout;

    _dropLabel = new QLabel( tr( "Drop INF Here" ) );
    _dropLabel->setStyleSheet( "background: white" );
    _dropLabel->setAlignment( Qt::AlignCenter );
    vLayout->addWidget( _dropLabel, 1 );

    QHBoxLayout* hLayout = new QHBoxLayout;

    _createVirtualDevice = new QCheckBox( tr( "Create virtual device" ) );
    _createVirtualDevice->setChecked( false );
    hLayout->addWidget( _createVirtualDevice );

    _virtualDeviceHardwareId = new QLineEdit;
    _virtualDeviceHardwareId->connect( _createVirtualDevice,
                                       SIGNAL( toggled( bool ) ),
                                       SLOT( setEnabled( bool ) ) );
    _virtualDeviceHardwareId->setEnabled( false );
    hLayout->addWidget( _virtualDeviceHardwareId, 1 );

    vLayout->addLayout( hLayout );

    hLayout = new QHBoxLayout;

    _forceCheck = new QCheckBox( tr( "Force" ) );
    hLayout->addWidget( _forceCheck );
    hLayout->addStretch( 1 );

    QPushButton* install = new QPushButton( tr( "Install INF" ) );
    hLayout->addWidget( install );
    install->connect( this,
                      SIGNAL( enabled( bool ) ),
                      SLOT( setEnabled( bool ) ) );
    connect( install, SIGNAL( clicked() ), SLOT( installDriver( ) ) );
    vLayout->addLayout( hLayout );

    emit enabled( false );

    setLayout( vLayout );
}

void DropWindow::dragEnterEvent( QDragEnterEvent* e )
{
    QString fileName( ( QChar* ) e->mimeData()->data( "FileNameW" ).data() );
    QFileInfo fileInfo( fileName );

    if( fileInfo.exists() &&
        0 == fileInfo.suffix().compare( "inf", Qt::CaseInsensitive ) )
    {
        e->accept();
    }
}

void DropWindow::dropEvent( QDropEvent* e )
{
    QString fileName( ( QChar* ) e->mimeData()->data( "FileNameW" ).data() );
    _infPath = fileName;

    _dropLabel->setText( _infPath.fileName() );

    emit enabled( true );

    e->acceptProposedAction();
}

bool DropWindow::createFakeDevice()
{
    qDebug() << "Creating fake device:" << _virtualDeviceHardwareId->text();
    bool result = false;

    HDEVINFO DeviceInfoSet = INVALID_HANDLE_VALUE;
    SP_DEVINFO_DATA DeviceInfoData;
    GUID ClassGUID;
    WCHAR ClassName[ MAX_CLASS_NAME_LEN ];
    WCHAR hwIdList[ LINE_LEN + 4 ];
    DWORD hwIdListSz;
    HRESULT hRes;
    BOOL bRes;

    QString nativePath
            = QDir::toNativeSeparators( _infPath.canonicalFilePath() );

    //
    // List of hardware ID's must be double zero-terminated
    //
    ZeroMemory( hwIdList, sizeof( hwIdList ) );
    hRes = StringCchCopyW( hwIdList,
                           LINE_LEN,
                           _virtualDeviceHardwareId->text().utf16() );
    if( FAILED( hRes ) )
    {
        qDebug( "Copy failed" );
        goto final;
    }

    //
    // Use the INF File to extract the Class GUID.
    //
    bRes = SetupDiGetINFClassW( nativePath.utf16(),
                                & ClassGUID,
                                ClassName,
                                sizeof( ClassName ) / sizeof( ClassName[ 0 ] ),
                                0 );

    if( ! bRes )
    {
        qDebug( "Could not get INF class" );
        goto final;
    }

    //
    // Create the container for the to-be-created Device Information Element.
    //
    DeviceInfoSet = SetupDiCreateDeviceInfoList( & ClassGUID, 0 );
    if( INVALID_HANDLE_VALUE == DeviceInfoSet )
    {
        qDebug( "Could not create " );
        goto final;
    }

    //
    // Now create the element.
    // Use the Class GUID and Name from the INF file.
    //
    DeviceInfoData.cbSize = sizeof( SP_DEVINFO_DATA );
    bRes = SetupDiCreateDeviceInfoW( DeviceInfoSet,
                                     ClassName,
                                     & ClassGUID,
                                     NULL,
                                     0,
                                     DICD_GENERATE_ID,
                                     & DeviceInfoData );
    if( ! bRes )
    {
        qDebug( "Could not create device info" );
        goto final;
    }

    //
    // Add the HardwareID to the Device's HardwareID property.
    //
    hwIdListSz = ( lstrlenW( hwIdList ) + 1 + 1 ) * sizeof( WCHAR );
    bRes = SetupDiSetDeviceRegistryPropertyW( DeviceInfoSet,
                                              & DeviceInfoData,
                                              SPDRP_HARDWAREID,
                                              ( LPBYTE ) hwIdList,
                                              hwIdListSz );
    if( ! bRes )
    {
        qDebug( "Could not set registry property" );
        goto final;
    }

    //
    // Transform the registry element into an actual devnode
    // in the PnP HW tree.
    //
    bRes = SetupDiCallClassInstaller( DIF_REGISTERDEVICE,
                                      DeviceInfoSet,
                                      & DeviceInfoData );
    if( ! bRes )
    {
        qDebug( "Could not install device" );
        goto final;
    }

    result = true;

final:
    if( INVALID_HANDLE_VALUE != DeviceInfoSet )
    {
        SetupDiDestroyDeviceInfoList( DeviceInfoSet );
    }

    return result;
}

void DropWindow::installDriver()
{
    if( _createVirtualDevice->isChecked() )
    {
        if( ! createFakeDevice() )
        {
            qDebug( "Failed to create fake device" );
            return;
        }
    }

    qDebug() << "Installing INF:" << _infPath.canonicalFilePath();

    QString nativePath
            = QDir::toNativeSeparators( _infPath.canonicalFilePath() );

    BOOL reboot;
    DWORD res = DriverPackageInstallW(
                nativePath.utf16(),
                _forceCheck->isChecked() ? DRIVER_PACKAGE_FORCE : 0x0,
                NULL,
                & reboot );

    qDebug( "Installed w/ code: %08X", res );
}
