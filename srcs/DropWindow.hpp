
#include <QWidget>

#include <QtCore/QFileInfo>

class QCheckBox;
class QLabel;
class QLineEdit;

class DropWindow : public QWidget
{
    Q_OBJECT

public:
    DropWindow();

protected:
    virtual void dragEnterEvent( QDragEnterEvent* e );
    virtual void dropEvent( QDropEvent* e );

private:
    bool createFakeDevice();

private slots:
    void installDriver();

signals:
    void enabled( bool );

private:
    QCheckBox*  _forceCheck;
    QCheckBox*  _createVirtualDevice;
    QLineEdit*  _virtualDeviceHardwareId;
    QLabel*     _dropLabel;
    QFileInfo   _infPath;
};
